package com.example.brandnewlauncher

import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.view.ViewTreeObserver.*
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MainActivity : AppCompatActivity() {
    // To mock heavy object creation
    private val sumValue = (1..10000000).map {
        it.div(8f).div(2f).plus(5.5f)
    }.sum()

    val viewModel by lazy {
        ViewModelProvider(this).get(MainVM::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        installSplashScreen()

        setContentView(R.layout.activity_main)
        findViewById<Button>(R.id.tv).text = sumValue.toString()

        val content: View = findViewById(android.R.id.content)

        content.viewTreeObserver.addOnPreDrawListener(object : OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                return if (viewModel.isReady) {
                    content.viewTreeObserver.removeOnPreDrawListener(this)
                    true
                } else {
                    false
                }
            }
        })

    }
}

class MainVM() : ViewModel() {
    var isReady : Boolean = false

    init {
        (object : CountDownTimer(10_000, 1_000) {
            override fun onTick(p0: Long) {

            }

            override fun onFinish() {
                isReady =  true
            }

        }).start()
    }
}